<?php

namespace Tests\Feature;

use App\Buyer;
use App\Product;
use App\Seller;
use App\Transaction;
use Tests\TestCase;
use App\Category;

class ExampleTest extends TestCase
{

    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testExistBuyerModel()
    {
        $buyer = new Buyer();

        $this->assertInstanceOf(Buyer::class, $buyer);
    }

    public function testExistSellerModel()
    {
        $seller = new Seller();

        $this->assertInstanceOf(Seller::class, $seller);
    }

    public function testExistTransactionModel()
    {
        $transaction = new Transaction();

        $this->assertInstanceOf(Transaction::class, $transaction);
    }

    public function testExistProductModel()
    {
        $product = new Product();

        $this->assertInstanceOf(Product::class, $product);
    }

    public function testExistCategoryModel()
    {
        $category = new Category();

        $this->assertInstanceOf(Category::class, $category);
    }

}

