<?php

namespace Tests\Feature;

use App\Category;
use App\Buyer;
use App\Product;
use App\Seller;
use App\Transaction;
use Tests\TestCase;


class RelationModelsTest extends TestCase
{

    public function testSellerAndProductRelationMethodsExists()
    {
        $seller = new Seller();
        $product = new Product();

        $products_method_exists = method_exists($seller, 'products');
        $sellers_method_exists = method_exists($product, 'sellers');

        $this->assertTrue(($products_method_exists and $sellers_method_exists));
    }

    public function testCategoryProductRelationMethodsExists()
    {
        $product = new Product();
        $category = new Category();

        $product_has_categories_method = method_exists($product, 'categories');
        $categories_has_products_method = method_exists($category, 'products');

        $both_methods_exist = ($product_has_categories_method and $categories_has_products_method);

        $this->assertTrue($condition=$both_methods_exist);
    }

    public function testExistsRelationMethodsBetweenBuyerAndTransaction()
    {
        $transaction = new Transaction();
        $buyer = new Buyer();

        $transaction_has_buyer_method = method_exists($transaction, 'buyer');
        $buyer_has_transactions_method = method_exists($buyer, 'transactions');

        $both_methods_exist = ($transaction_has_buyer_method and $buyer_has_transactions_method);

        $this->assertTrue($condition=$both_methods_exist);
    }

}
