<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    // |products| >- |seller|
    public function sellers()
    {
        $this->belongsTo($related=Seller::class, $foreignKey='seller_id', $ownerKey='id');
    }

    // |products| >-< |categories|
    public function categories()
    {
        return $this->belongsToMany(
            $related=Category::class,
            $table='category_product',
            $foreignPivotKey='category_id',
            $relatedPivotKey='product_id'
        );
    }
}
