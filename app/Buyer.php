<?php
namespace App;

class Buyer extends User
{
    //|buyer|-<|transactions|
    public function transactions()
    {
        return $this->hasMany($related=Transaction::class, $foreignKey='buyer_id', $localKey='id');
    }

}