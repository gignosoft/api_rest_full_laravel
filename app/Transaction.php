<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    protected $table = 'transactions';

    //|transactions|>-|Model_buyer(User)|
    public function buyer()
    {
        return $this->belongsTo($related=Buyer::class, $foreignKey='buyer_id', $ownerKey='id');
    }
}
