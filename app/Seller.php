<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Seller extends User
{
    // |seller| -< |products|
    public function products()
    {
        $this->hasMany($related=Product::class, $foreignKey='seller_id', $localKey='id');
    }
}
