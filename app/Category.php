<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';


    // |categories| >-< |products|
    public function products()
    {
        return $this->belongsToMany(
            $related=Product::class,
            $table='category_product',
            $foreignPivotKey='product_id',
            $relatedPivotKey='category_id'
        );
    }
}
