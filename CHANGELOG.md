# Changelog

Release: 18.6.0
Friday 2018, Jan 19

* **Feature** [Create relationchip between Product y Category & buyer y transaction](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/32)

Release: 18.5.0
Wednesday 2018, Jan 17

* **Feature** [Create Category model](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/28)

Release: 18.4.0
Wednesday 2018, Jan 16

* **Feature** [Create Category model](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/27)

## Release: 18.3.0
Thursday 2018, Jan 04

* **Feature** [Create Product model](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/24)

## Release: 18.2.0
Monday 2018, Jan 01

* **Feature** [Create Buyer model](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/12)

## Release: 18.1.0
Monday 2018, Jan 01

* **Feature** [Create a seller model](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/13)

## Release: 18.0.0
Monday 2018, Jan 01

* **Feature** [Create category_product migration](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/14)

## Release: 17.4.0
Sunday 2017, Dic 31

* **Feature** [Create-categories-migrations](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/10)

## Release: 17.3.0
Sunday 2017, Dic 31

* **Feature** [Create transaction migrationes](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/8)

## Release: 17.2.0
Sunday 2017, Dic 31

* **Feature** [Create product migrations](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/9)

## Release: 17.1.0
Thursday 2017, Dic 28

* **Feature** [Update User migration and model](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/7)

## Hotfix: 17.0.3
Thursday 2017, Dic 14

* **Hotfix** [Add docker-compose.yml to .gitignore ](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/6)

## Hotfix: 17.0.2
Thursday 2017, Dic 14

* **Hotfix** [Delete docker-compose.yml an add to .gitignore ](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/5)

## Hotfix: 17.0.1
Wednesday 2017, Dic 13

* **Hotfix** [Add docker-compose.yml.example ](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/4/hotfix-1701-ap-3/diffs)

## Release: 17.0.0
Wednesday 2017, Dic 13

* **Feature** [Create the laravel environment to begin programing](https://bitbucket.org/gignosoft/api_rest_full_laravel/pull-requests/1/feature-set-environment-ap-1/diff)
